var G_vmlCanvasManager;

function ShapesPreloader(completeCallback, options) {

	var _self = this;
	var canvas;
	var $canvas;
	var ctx;

	var stageHeight;
	var stageWidth;
	var totalShapes = 48;

	var loadProgress = 0;
	

	var shapesOrder = [];
	var shapesRevealed = [];

	var originalShapesWidth = 517;
	var originalShapesHeight = 448;

	// for min delaying effect if real loading is too fast
	var delayedTimeoutTime = 200;
	var delayedProgressStep = 0.05;
	//var minDurationMSec = 3000;
	var currentTimestamp = 0;
	var delayedProgress = 0;

	var delayTimeout;

	if(options && options.color) {
	
		var mainColor = options.color;

	} else {

		mainColor = '#000000'
	}

	var completeCallback = completeCallback;
	if(!completeCallback) return;
	
	$canvas = $('<canvas class="preloaderCanvas"></canvas>');
	$('body').append($canvas);

	canvas = $canvas.get(0);


	if(G_vmlCanvasManager != undefined) 
		G_vmlCanvasManager.initElement(canvas);

	ctx = canvas.getContext('2d');
		

	shapesOrder = _.shuffle(_.range(totalShapes));
	
	resizeHandler();
	$(window).bind('resize', resizeHandler);

	function resizeHandler() {

		stageHeight = $(window).height();
		stageWidth = $(window).width();

		$canvas.css('width', stageWidth);
		$canvas.css('height', stageHeight);
		canvas.width = stageWidth;
		canvas.height = stageHeight;
		
		redraw();
	}

	
	function redraw() {

		canvas.width = stageWidth;
		
		ctx.globalCompositeOperation = "xor";
		ctx.fillStyle = mainColor;
		ctx.fillRect(0,0,stageWidth,stageHeight);
		
		ctx.save();
		
		var targetScale = 1;
		
		if(stageWidth >= 480 && stageWidth < 768) {
			targetScale = 1;
		} else if(stageWidth >= 768 && stageWidth < 960) {
			targetScale = 1.5;
		} else if(stageWidth >= 960) {
			targetScale = 2;
		}

		ctx.translate(stageWidth/2 - originalShapesWidth*targetScale/2,stageHeight/2 - originalShapesHeight*targetScale/2);
		ctx.scale(targetScale,targetScale);
		ctx.strokeStyle = 'rgba(0,0,0,0)';
		ctx.lineCap = 'butt';
		ctx.lineJoin = 'miter';
		ctx.miterLimit = 4;
		ctx.save();

		for(var i = 0; i < shapesRevealed.length; i++) {
			
			_self['drawShape_' + shapesRevealed[i]]();

		}

		ctx.restore();

	}

	// this will update graphics depending on delay effect
	function updateShapesProgress() {

		if(loadProgress <= delayedProgress) return;

		var diff = loadProgress - delayedProgress;

		if(diff > delayedProgressStep) {

			diff = delayedProgressStep;
			clearInterval(delayTimeout);
			delayTimeout = setTimeout(updateShapesProgress, delayedTimeoutTime);
		}

		delayedProgress += diff;

		var shapesProgress = shapesRevealed.length/totalShapes;

		
		// how many shapes we need to show?
		var numToReveal = Math.floor((delayedProgress - shapesProgress)*totalShapes);

		if(numToReveal > 1) {

			for(var i = 0; i < numToReveal; i++) {

				shapesRevealed.push(shapesOrder.shift());
			}

			redraw();
		}
		
		if(delayedProgress == 1) {
			
			clearInterval(delayTimeout);
			completeCallback();
		}

	}

	/* "Public" methods */
		
	this.destroy = function() {

		$canvas.fadeOut('slow', function() {

			$(window).unbind('resize', resizeHandler);
			$canvas.remove();

		});

	}

	// takes "real" loading percentage progress (0-1)
	this.updateProgress = function(progress) {

		loadProgress = progress;
		if(loadProgress > 1)
			loadProgress = 1;


		updateShapesProgress();
	
	}


	this.drawShape_0 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(200.309,247.326);
		ctx.bezierCurveTo(200.309,275.582,200.309,303.749,200.309,332.144);
		ctx.bezierCurveTo(172.142,332.144,143.999,332.144,115.64,332.144);
		ctx.bezierCurveTo(115.64,303.899,115.64,275.736,115.64,247.32600000000002);
		ctx.bezierCurveTo(143.831,247.326,171.973,247.326,200.309,247.326);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

	}
	
	this.drawShape_1 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(401.496,115.191);
		ctx.bezierCurveTo(401.496,143.43,401.496,171.496,401.496,199.856);
		ctx.bezierCurveTo(373.216,199.856,344.981,199.856,316.519,199.856);
		ctx.bezierCurveTo(316.519,171.689,316.519,143.543,316.519,115.19099999999999);
		ctx.bezierCurveTo(344.857,115.191,373.098,115.191,401.496,115.191);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

	}
	
	this.drawShape_2 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(227.562,72.143);
		ctx.bezierCurveTo(255.77800000000002,72.143,283.844,72.143,312.144,72.143);
		ctx.bezierCurveTo(312.144,100.443,312.144,128.679,312.144,157.09300000000002);
		ctx.bezierCurveTo(283.948,157.09300000000002,255.885,157.09300000000002,227.562,157.09300000000002);
		ctx.bezierCurveTo(227.562,128.757,227.562,100.531,227.562,72.143);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}
	
	this.drawShape_3 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(110.999,174.557);
		ctx.bezierCurveTo(74.10399999999998,174.557,37.52799999999999,174.557,0.9519999999999982,174.557);
		ctx.bezierCurveTo(0.6349999999999982,174.172,40.300999999999995,134.50599999999997,58.885,115.827);
		ctx.bezierCurveTo(60.324,114.38,61.745999999999995,113.746,63.78,113.803);
		ctx.bezierCurveTo(68.56,113.93599999999999,73.346,113.846,78.623,113.846);
		ctx.bezierCurveTo(77.953,112.65,77.48400000000001,111.791,76.99600000000001,110.944);
		ctx.bezierCurveTo(70.141,99.047,73.924,83.935,85.56,76.744);
		ctx.bezierCurveTo(97.098,69.615,112.218,72.863,119.81200000000001,84.103);
		ctx.bezierCurveTo(127.47900000000001,95.451,124.76500000000001,110.814,113.50200000000001,118.72399999999999);
		ctx.bezierCurveTo(111.516,120.11899999999999,110.94000000000001,121.53899999999999,110.953,123.81899999999999);
		ctx.bezierCurveTo(111.038,139.545,110.998,155.271,110.998,170.99699999999999);
		ctx.bezierCurveTo(110.999,172.117,110.999,173.235,110.999,174.557);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}


	this.drawShape_4 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(406.084,272.722);
		ctx.bezierCurveTo(442.98,272.722,479.485,272.722,516.427,272.722);
		ctx.bezierCurveTo(515.7040000000001,273.572,515.193,274.26599999999996,514.59,274.86899999999997);
		ctx.bezierCurveTo(495.75300000000004,293.72299999999996,476.934,312.59099999999995,458.002,331.349);
		ctx.bezierCurveTo(456.841,332.5,454.82800000000003,333.256,453.164,333.349);
		ctx.bezierCurveTo(448.417,333.615,443.645,333.443,439.185,333.443);
		ctx.bezierCurveTo(440.578,337.865,442.426,341.972,443.122,346.265);
		ctx.bezierCurveTo(445.277,359.574,434.771,372.627,421.024,374.222);
		ctx.bezierCurveTo(407.155,375.83099999999996,394.06600000000003,365.308,393.012,351.733);
		ctx.bezierCurveTo(392.239,341.754,395.865,333.81,404.088,328.111);
		ctx.bezierCurveTo(405.70300000000003,326.99199999999996,406.118,325.791,406.112,323.983);
		ctx.bezierCurveTo(406.06100000000004,308.264,406.083,292.545,406.084,276.825);
		ctx.bezierCurveTo(406.084,275.582,406.084,274.337,406.084,272.722);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}
	

	this.drawShape_5 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(218.401,161.236);
		ctx.bezierCurveTo(240.662,161.213,258.567,178.99099999999999,258.629,201.18099999999998);
		ctx.bezierCurveTo(258.692,223.545,240.794,241.60799999999998,218.51700000000002,241.66299999999998);
		ctx.bezierCurveTo(196.36200000000002,241.71999999999997,178.13000000000002,223.47099999999998,178.18900000000002,201.29799999999997);
		ctx.bezierCurveTo(178.247,179.153,196.206,161.26,218.401,161.236);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}
	
	this.drawShape_6 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(338.657,245.821);
		ctx.bezierCurveTo(338.68899999999996,268.164,320.69599999999997,286.204,298.424,286.158);
		ctx.bezierCurveTo(276.346,286.113,258.282,268.057,258.231,245.983);
		ctx.bezierCurveTo(258.18,223.775,276.30899999999997,205.666,298.575,205.684);
		ctx.bezierCurveTo(320.752,205.704,338.625,223.6,338.657,245.821);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}
		

	this.drawShape_7 = function() { 
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(165.406,336.57);
		ctx.bezierCurveTo(177.107,336.57,188.531,336.57,200.227,336.57);
		ctx.bezierCurveTo(200.227,373.402,200.227,410.172,200.227,447.127);
		ctx.bezierCurveTo(188.661,447.127,177.15800000000002,447.127,165.406,447.127);
		ctx.bezierCurveTo(165.406,410.324,165.406,373.634,165.406,336.57);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_8 = function() { 
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(351.458,110.534);
		ctx.bezierCurveTo(339.759,110.534,328.24600000000004,110.534,316.55100000000004,110.534);
		ctx.bezierCurveTo(316.55100000000004,73.71100000000001,316.55100000000004,37.098,316.55100000000004,0.26900000000000546);
		ctx.bezierCurveTo(328.16400000000004,0.26900000000000546,339.67500000000007,0.26900000000000546,351.458,0.26900000000000546);
		ctx.bezierCurveTo(351.458,36.867,351.458,73.555,351.458,110.534);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_9 = function() { 
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(382.507,204.499);
		ctx.bezierCurveTo(382.507,241.279,382.507,277.978,382.507,314.767);
		ctx.bezierCurveTo(370.785,314.767,359.273,314.767,347.578,314.767);
		ctx.bezierCurveTo(347.578,278.008,347.578,241.405,347.578,204.499);
		ctx.bezierCurveTo(359.162,204.499,370.733,204.499,382.507,204.499);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_10 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(169.437,242.935);
		ctx.bezierCurveTo(157.78,242.935,146.26600000000002,242.935,134.53000000000003,242.935);
		ctx.bezierCurveTo(134.53000000000003,206.18200000000002,134.53000000000003,169.497,134.53000000000003,132.631);
		ctx.bezierCurveTo(146.16900000000004,132.631,157.68400000000003,132.631,169.437,132.631);
		ctx.bezierCurveTo(169.437,169.389,169.437,206.078,169.437,242.935);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_11 = function() {
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(516.501,115.382);
		ctx.bezierCurveTo(516.501,127.028,516.501,138.54500000000002,516.501,150.311);
		ctx.bezierCurveTo(479.82399999999996,150.311,443.139,150.311,406.163,150.311);
		ctx.bezierCurveTo(406.163,138.749,406.163,127.173,406.163,115.382);
		ctx.bezierCurveTo(442.952,115.382,479.642,115.382,516.501,115.382);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_12 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(406.255,267.941);
		ctx.bezierCurveTo(406.255,256.28,406.255,244.86999999999998,406.255,233.236);
		ctx.bezierCurveTo(443.01099999999997,233.236,479.606,233.236,516.473,233.236);
		ctx.bezierCurveTo(516.473,244.68699999999998,516.473,256.174,516.473,267.941);
		ctx.bezierCurveTo(479.884,267.941,443.219,267.941,406.255,267.941);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_13 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(110.909,297.042);
		ctx.bezierCurveTo(110.909,308.799,110.909,320.215,110.909,331.822);
		ctx.bezierCurveTo(74.13400000000001,331.822,37.53200000000001,331.822,0.6600000000000108,331.822);
		ctx.bezierCurveTo(0.6600000000000108,320.25600000000003,0.6600000000000108,308.77,0.6600000000000108,297.04200000000003);
		ctx.bezierCurveTo(37.367,297.042,74.046,297.042,110.909,297.042);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_14 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(349.81,343.817);
		ctx.bezierCurveTo(349.81,365.479,349.81,386.794,349.81,408.35);
		ctx.bezierCurveTo(331.236,408.35,312.76300000000003,408.35,294.027,408.35);
		ctx.bezierCurveTo(294.027,386.957,294.027,365.49600000000004,294.027,343.817);
		ctx.bezierCurveTo(312.556,343.817,331.02,343.817,349.81,343.817);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_15 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(222.973,38.98);
		ctx.bezierCurveTo(222.973,60.580999999999996,222.973,81.898,222.973,103.46600000000001);
		ctx.bezierCurveTo(204.33300000000003,103.46600000000001,185.786,103.46600000000001,167.079,103.46600000000001);
		ctx.bezierCurveTo(167.079,81.95100000000001,167.079,60.56300000000001,167.079,38.980000000000004);
		ctx.bezierCurveTo(185.69,38.98,204.159,38.98,222.973,38.98);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	

	this.drawShape_16 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(402.155,0);
		ctx.bezierCurveTo(416.042,0.072,427.34099999999995,11.496,427.25899999999996,25.382);
		ctx.bezierCurveTo(427.17799999999994,39.214,415.71599999999995,50.562,401.85599999999994,50.536);
		ctx.bezierCurveTo(387.81499999999994,50.509,376.59599999999995,39.085,376.7099999999999,24.93);
		ctx.bezierCurveTo(376.823,10.962,388.087,-0.073,402.155,0);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_17 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(89.824,421.949);
		ctx.bezierCurveTo(89.872,407.93100000000004,101.045,396.83000000000004,115.087,396.845);
		ctx.bezierCurveTo(129.041,396.86100000000005,140.351,408.12800000000004,140.374,422.03400000000005);
		ctx.bezierCurveTo(140.397,435.97,128.81,447.475,114.842,447.38800000000003);
		ctx.bezierCurveTo(100.946,447.301,89.776,435.943,89.824,421.949);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_18 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(160.95,446.982);
		ctx.bezierCurveTo(156.13299999999998,446.982,151.403,446.982,146.487,446.982);
		ctx.bezierCurveTo(146.487,410.101,146.487,373.418,146.487,336.545);
		ctx.bezierCurveTo(151.32999999999998,336.545,156.051,336.545,160.95,336.545);
		ctx.bezierCurveTo(160.95,373.377,160.95,410.074,160.95,446.982);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_19 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(355.95,0.264);
		ctx.bezierCurveTo(360.73199999999997,0.264,365.373,0.264,370.279,0.264);
		ctx.bezierCurveTo(370.279,36.953,370.279,73.631,370.279,110.606);
		ctx.bezierCurveTo(365.58,110.606,360.876,110.606,355.95,110.606);
		ctx.bezierCurveTo(355.95,73.813,355.95,37.125,355.95,0.264);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_20 = function() {
	 ctx.save();;
		ctx.beginPath();
		ctx.moveTo(387.024,204.468);
		ctx.bezierCurveTo(391.88100000000003,204.468,396.526,204.468,401.378,204.468);
		ctx.bezierCurveTo(401.378,241.296,401.378,277.964,401.378,314.79999999999995);
		ctx.bezierCurveTo(396.575,314.79999999999995,391.93,314.79999999999995,387.024,314.79999999999995);
		ctx.bezierCurveTo(387.024,278.121,387.024,241.457,387.024,204.468);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	
	this.drawShape_21 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(130.085,242.876);
		ctx.bezierCurveTo(125.33200000000001,242.876,120.68900000000001,242.876,115.77000000000001,242.876);
		ctx.bezierCurveTo(115.77000000000001,206.238,115.77000000000001,169.572,115.77000000000001,132.626);
		ctx.bezierCurveTo(120.433,132.626,125.14500000000001,132.626,130.085,132.626);
		ctx.bezierCurveTo(130.085,169.344,130.085,206.02,130.085,242.876);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_22 = function() {
	 ctx.save();;
		ctx.beginPath();
		ctx.moveTo(451.456,111.507);
		ctx.bezierCurveTo(435.93,111.507,421.074,111.507,405.677,111.507);
		ctx.bezierCurveTo(412.99100000000004,90.797,420.20300000000003,70.381,427.625,49.367000000000004);
		ctx.bezierCurveTo(435.667,70.339,443.468,90.678,451.456,111.507);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_23 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(89.336,397.914);
		ctx.bezierCurveTo(81.202,376.706,73.41,356.392,65.525,335.835);
		ctx.bezierCurveTo(80.815,335.835,95.762,335.835,111.25900000000001,335.835);
		ctx.bezierCurveTo(104.017,356.341,96.835,376.68,89.336,397.914);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_24 = function() { 
		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(406.319,155.085);
		ctx.bezierCurveTo(417.971,155.085,429.36400000000003,155.085,440.921,155.085);
		ctx.bezierCurveTo(440.921,166.761,440.921,178.245,440.921,189.91400000000002);
		ctx.bezierCurveTo(429.352,189.91400000000002,417.959,189.91400000000002,406.319,189.91400000000002);
		ctx.bezierCurveTo(406.319,178.342,406.319,166.864,406.319,155.085);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_25 = function() { 

		ctx.save();;
		ctx.beginPath();
		ctx.moveTo(406.457,194.251);
		ctx.bezierCurveTo(418.01,194.251,429.39799999999997,194.251,440.904,194.251);
		ctx.bezierCurveTo(440.904,205.74200000000002,440.904,217.041,440.904,228.522);
		ctx.bezierCurveTo(429.378,228.522,417.998,228.522,406.457,228.522);
		ctx.bezierCurveTo(406.457,217.049,406.457,205.759,406.457,194.251);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_26 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(294.044,305.074);
		ctx.bezierCurveTo(305.589,305.074,316.895,305.074,328.37899999999996,305.074);
		ctx.bezierCurveTo(328.37899999999996,316.446,328.37899999999996,327.647,328.37899999999996,339.14);
		ctx.bezierCurveTo(317.04499999999996,339.14,305.66999999999996,339.14,294.044,339.14);
		ctx.bezierCurveTo(294.044,327.932,294.044,316.661,294.044,305.074);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }


	this.drawShape_27 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(71.515,218.865);
		ctx.bezierCurveTo(71.515,230.191,71.515,241.39000000000001,71.515,252.87400000000002);
		ctx.bezierCurveTo(60.176,252.87400000000002,48.803,252.87400000000002,37.187,252.87400000000002);
		ctx.bezierCurveTo(37.187,241.657,37.187,230.38600000000002,37.187,218.865);
		ctx.bezierCurveTo(48.613,218.865,59.978,218.865,71.515,218.865);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_28 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(445.615,194.217);
		ctx.bezierCurveTo(457.002,194.217,468.206,194.217,479.656,194.217);
		ctx.bezierCurveTo(479.656,205.613,479.656,216.90200000000002,479.656,228.45000000000002);
		ctx.bezierCurveTo(468.45300000000003,228.45000000000002,457.173,228.45000000000002,445.615,228.45000000000002);
		ctx.bezierCurveTo(445.615,217.2,445.615,205.92,445.615,194.217);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_29 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(188.796,107.914);
		ctx.bezierCurveTo(200.25199999999998,107.914,211.435,107.914,222.832,107.914);
		ctx.bezierCurveTo(222.832,119.361,222.832,130.643,222.832,142.185);
		ctx.bezierCurveTo(211.62099999999998,142.185,200.357,142.185,188.796,142.185);
		ctx.bezierCurveTo(188.796,130.915,188.796,119.564,188.796,107.914);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_30 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(76.287,218.792);
		ctx.bezierCurveTo(87.733,218.792,98.932,218.792,110.339,218.792);
		ctx.bezierCurveTo(110.339,230.167,110.339,241.37,110.339,252.865);
		ctx.bezierCurveTo(99.122,252.865,87.848,252.865,76.287,252.865);
		ctx.bezierCurveTo(76.287,241.667,76.287,230.397,76.287,218.792);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	
	this.drawShape_31 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(354.346,343.836);
		ctx.bezierCurveTo(365.841,343.836,377.044,343.836,388.442,343.836);
		ctx.bezierCurveTo(388.442,355.21000000000004,388.442,366.414,388.442,377.906);
		ctx.bezierCurveTo(377.192,377.906,365.918,377.906,354.346,377.906);
		ctx.bezierCurveTo(354.346,366.671,354.346,355.404,354.346,343.836);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_32 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(162.84,64.824);
		ctx.bezierCurveTo(151.29500000000002,64.824,140.169,64.824,128.618,64.824);
		ctx.bezierCurveTo(134.50799999999998,49.458,140.328,34.277,146.432,18.353);
		ctx.bezierCurveTo(152.044,34.25,157.405,49.432,162.84,64.824);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_33 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(388.249,382.708);
		ctx.bezierCurveTo(382.387,397.994,376.64500000000004,412.968,370.535,428.90000000000003);
		ctx.bezierCurveTo(364.915,413.009,359.61100000000005,398.01500000000004,354.197,382.708);
		ctx.bezierCurveTo(365.557,382.708,376.604,382.708,388.249,382.708);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_34 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(454.913,111.388);
		ctx.bezierCurveTo(459.627,98.043,464.21500000000003,85.053,469.107,71.2);
		ctx.bezierCurveTo(474.38500000000005,84.94200000000001,479.39000000000004,97.973,484.543,111.388);
		ctx.bezierCurveTo(474.508,111.388,464.933,111.388,454.913,111.388);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_35 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(62.039,335.908);
		ctx.bezierCurveTo(57.315,349.279,52.7,362.34200000000004,47.841,376.093);
		ctx.bezierCurveTo(42.55,362.30100000000004,37.542,349.247,32.424,335.908);
		ctx.bezierCurveTo(42.294,335.908,51.886,335.908,62.039,335.908);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_36 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(312.25,67.852);
		ctx.bezierCurveTo(300.521,67.852,289.422,67.852,277.624,67.852);
		ctx.bezierCurveTo(283.15700000000004,58.051,288.519,48.55200000000001,294.199,38.491);
		ctx.bezierCurveTo(300.26,48.351,306.093,57.837,312.25,67.852);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_37 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(239.203,379.695);
		ctx.bezierCurveTo(233.698,389.433,228.302,398.977,222.744,408.807);
		ctx.bezierCurveTo(216.726,399.004,210.908,389.52500000000003,204.874,379.695);
		ctx.bezierCurveTo(216.399,379.695,227.605,379.695,239.203,379.695);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	

	this.drawShape_38 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(125.975,64.821);
		ctx.bezierCurveTo(118.34599999999999,64.821,111.28,64.821,103.80499999999999,64.821);
		ctx.bezierCurveTo(107.591,54.925,111.294,45.244,115.317,34.729);
		ctx.bezierCurveTo(118.999,45.127,122.428,54.809,125.975,64.821);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	

	this.drawShape_39 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(401.633,412.6);
		ctx.bezierCurveTo(397.945,402.14500000000004,394.541,392.49300000000005,391.053,382.601);
		ctx.bezierCurveTo(398.49,382.601,405.539,382.601,413.1,382.601);
		ctx.bezierCurveTo(409.34,392.438,405.661,402.061,401.633,412.6);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		

	this.drawShape_40 = function() { ctx.save();;
		ctx.beginPath();
		ctx.moveTo(464.669,165.073);
		ctx.bezierCurveTo(458.441,168.895,452.71799999999996,172.40800000000002,446.60699999999997,176.15800000000002);
		ctx.bezierCurveTo(446.60699999999997,168.95200000000003,446.60699999999997,162.191,446.60699999999997,154.89800000000002);
		ctx.bezierCurveTo(452.599,158.274,458.327,161.501,464.669,165.073);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		
	this.drawShape_41 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(70.294,292.383);
		ctx.bezierCurveTo(64.12899999999999,288.897,58.379,285.647,52.306,282.212);
		ctx.bezierCurveTo(58.385,278.483,64.16499999999999,274.937,70.294,271.17699999999996);
		ctx.bezierCurveTo(70.294,278.368,70.294,285.124,70.294,292.383);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
	
	this.drawShape_42 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(304.17,17.85);
		ctx.bezierCurveTo(300.906,23.623,297.822,29.080000000000002,294.53000000000003,34.902);
		ctx.bezierCurveTo(290.975,29.106,287.636,23.662,284.072,17.85);
		ctx.bezierCurveTo(290.877,17.85,297.237,17.85,304.17,17.85);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		
	this.drawShape_43 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(222.434,412.393);
		ctx.bezierCurveTo(226.005,418.217,229.287,423.56899999999996,232.831,429.34599999999995);
		ctx.bezierCurveTo(225.988,429.34599999999995,219.666,429.34599999999995,212.85399999999998,429.34599999999995);
		ctx.bezierCurveTo(216.096,423.607,219.13,418.238,222.434,412.393);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); 
	}
	
	this.drawShape_44 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(76.287,258.31);
		ctx.bezierCurveTo(87.733,258.31,98.932,258.31,110.339,258.31);
		ctx.bezierCurveTo(110.339,269.685,110.339,280.888,110.339,292.383);
		ctx.bezierCurveTo(99.122,292.383,87.848,292.383,76.287,292.383);
		ctx.bezierCurveTo(76.287,281.185,76.287,269.916,76.287,258.31);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		
	this.drawShape_45 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(110.477,179.448);
		ctx.bezierCurveTo(110.477,190.774,110.477,201.973,110.477,213.457);
		ctx.bezierCurveTo(73.98400000000001,213.457,37.382000000000005,213.457,0,213.457);
		ctx.bezierCurveTo(0,202.23999999999998,0,190.969,0,179.44799999999998);
		ctx.bezierCurveTo(36.77,179.448,73.345,179.448,110.477,179.448);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		
	this.drawShape_46 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(128.711,69.194);
		ctx.bezierCurveTo(140.167,69.194,151.35000000000002,69.194,162.747,69.194);
		ctx.bezierCurveTo(162.747,80.641,162.747,91.923,162.747,103.465);
		ctx.bezierCurveTo(151.536,103.465,140.27200000000002,103.465,128.711,103.465);
		ctx.bezierCurveTo(128.711,92.196,128.711,80.845,128.711,69.194);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); }
		
	this.drawShape_47 = function() {
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(289.543,289.413);
		ctx.bezierCurveTo(289.543,317.669,289.543,345.836,289.543,374.231);
		ctx.bezierCurveTo(261.376,374.231,233.233,374.231,204.87400000000002,374.231);
		ctx.bezierCurveTo(204.87400000000002,345.986,204.87400000000002,317.823,204.87400000000002,289.413);
		ctx.bezierCurveTo(233.065,289.413,261.207,289.413,289.543,289.413);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore(); 
	}

}