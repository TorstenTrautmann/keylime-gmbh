var App = angular.module('ngApp', 
	['ngSanitize']
).config(function( $locationProvider) {
	
}).run(function() {
	FastClick.attach(document.body);
});

App.controller('hContentsController',['$scope','pages', function($scope,pages){
	// console.log("init contents");
}]);

App.controller('hSectionController',['$scope','pages', function($scope,pages){
	// console.log("init section");
}]);


App.controller('collectionsController',['$scope','pages','$timeout','$location','$anchorScroll', function($scope,pages,$timeout,$location,$anchorScroll){
	this.activeColl = null;
	var scopeThis = this;
	this.isLoading = false;
	this.collDataset = null;
	this.scrollState = null;
	if($location.path() !== ""){
		var temp = $location.path().replace("/","");
		if($("#"+temp).hasClass("single-collection")){
			$("#collections").scrollTop($("#"+temp).offset().top);
		}
	}
	$scope.$on( "$locationChangeSuccess", function() {
		if ($location.path().indexOf('img') > -1 && !$("body").hasClass("modal-on")) {
			var elClass = $location.path().replace('/', '');
			$timeout(function(){
				$('.'+elClass).trigger('click');
			});
		} else if (($location.path() == '/' || $location.path() == '') && $("body").hasClass("modal-on")) {
			scopeThis.closeColl();
		}
	});
	this.scrollToSpot = function(passedImage){
		var newScroll = $(".singleCollectionViewer .image-scroll li.img-"+passedImage).offset().top - 100;

		if(newScroll > 0){
			// console.log("Success! scrolling to " + newScroll);
			$(".singleCollectionViewer").scrollTop(newScroll);
			$(".image-scroll").addClass("scrollSet");
		} else {
			// console.log("Failure to find image " + passedImage);
			$(".singleCollectionViewer").scrollTop(0);
			scopeThis.scrollToSpot(passedImage);
		}
	};

	this.loadColl = function(passedCollection,passedImage){
		$("body").addClass("modal-on");
		this.scrollState = $("#collections").scrollTop();
		this.activeColl = passedCollection;
		this.activeImg = passedImage;
		this.isLoading = true;
		pages.getCollImages(passedCollection).then(function(data) {
			scopeThis.collDataset = data.data;
			// console.log(data.data);
			var newHash = 'img-' + passedImage;
			
			$location.path('img-' + passedImage);
			$timeout(function(){
				// console.log($(".singleCollectionViewer .image-scroll li.img-"+passedImage).offset().top - 100);
				// $(".singleCollectionViewer").scrollTop($(".singleCollectionViewer .image-scroll li.img-"+passedImage).offset().top - 100);
				// scopeThis.scrollToSpot(passedImage);
				$(".image-scroll").addClass("scrollSet");
				$anchorScroll(newHash);

				$(".info-toggle").on("click",function(){
					if(!$(this).closest(".item-content").hasClass("data-loaded")){
						var products = $(this).attr("data-products");
						var productList = products.split("|");
						for(var i=0;i<productList.length;i++){
							scopeThis.getProductInfo($(this).parent(),productList[i]);
							// console.log(productList[i]);
						}
						$(this).closest(".item-content").addClass("data-loaded");
					}
					$(this).closest(".image-item").toggleClass("show-info");
				});
			},150);
		});
	};

	this.closeColl = function(){
		this.activeColl = null;
		this.collDataset = null;
		$("body").removeClass("modal-on");
		$location.path('');
		$timeout(function(){
			if(scopeThis.scrollState !== null){
				 $("#collections").scrollTop(scopeThis.scrollState);
				// $("html, body").animate({ scrollTop: scopeThis.scrollState }, 1);
			}
			scopeThis.scrollState = null;
		});
	};

	this.productString = function(passedProducts){
		if(passedProducts === false){
			return false;
		} else {
			var finalString = "";
			for(var i = 0; i<passedProducts.length;i++){
				if(i>0){ finalString+="|";}
				finalString+=passedProducts[i].product_id;
			}
			return finalString;
		}
	};

	this.getProductInfo = function(containObj,passedURL){
		$.getJSON(storeURL+'/products/' + passedURL + '.json?callback=?').done(
			function(x){
				var singleQuantity = x.product.variants[0].inventory_quantity;
				var purchaseOptions = '<div id="button-add"><div class="button" data-id="' + x.product.variants[0].id + '">Add to Bag</div></div>';
				if(singleQuantity < 1){
					purchaseOptions = '<div class="soldout-container"><div class="soldout-button no-events">SOLD OUT</div></div>';
				}
				containObj.find(".item-info-inner").append('<div class="product-preview clearfix"><a href="' + storeURL +'/products/' + x.product.handle + '?variant=' + x.product.variants[0].id +  '"><img src="' + x.product.image.src + '"></a><div class="prod-info"><h1><a href="' + storeURL +'/products/' + x.product.handle + '?variant=' + x.product.variants[0].id +  '">' + x.product.title + '</a></h1><small>$' + x.product.variants[0].price + '</small>' + purchaseOptions + '</div>');
			}
		);

	};
	
	collsCtrl = this;
	collsCtrl.scrollPositions=[];
	collsCtrl.lastScrollY = 0;
	collsCtrl.activeSection = '';
	$scope.pages = pages;

	$timeout(function() {
		collsCtrl.lastScrollY = window.scrollY;
		collsCtrl.onResize();
	});

	this.onResize = function() {
		this.footerOffset = $("footer").offset().top;
		this.winH = $(window).height();
		collsCtrl.scrollPositions=[];
		$timeout(function() {
			angular.forEach($('.single-collection'), function(item, i) {
				collsCtrl.scrollPositions[i] = [$(window).height()*i, $(item).attr("rel")];
			});

			collsCtrl.triggerScroll();
		}, 1000);
	};
	this.onScroll = function() {
		collsCtrl.lastScrollY = window.scrollY;
		collsCtrl.requestTick();
	};

	this.requestTick = function() {
		if(!collsCtrl.ticking) {
			collsCtrl.ticking = true;
			requestAnimationFrame(collsCtrl.triggerScroll);
		}
	};
	this.footerOffset = 0;
	this.winH = 2000;
	this.triggerScroll = function(){
		var newSection = "";
		var scrollAmt = $("#collections").scrollTop();
		$(".nav-secondary").css({"transform":"translateY(-" + scrollAmt + "px"});
		// console.log(collsCtrl.scrollPositions);
		angular.forEach(collsCtrl.scrollPositions, function(item, i) {
			var next = i+1;
			var prev = i-1;
			if (i == collsCtrl.scrollPositions.length - 1) {
				if(scrollAmt + 150 > collsCtrl.scrollPositions[i][0]) {
					newSection = item[1];
				}
			} else {
				if(scrollAmt + 150 > collsCtrl.scrollPositions[i][0] && scrollAmt + 150 <= collsCtrl.scrollPositions[next][0]) {
					newSection = item[1];
				}
			}
		});
		$timeout(function() {
			if (collsCtrl.activeSection  !== newSection) {
				collsCtrl.activeSection = newSection;
				// window.location.hash = '#' + collsCtrl.activeSection;
				pages.activeSection = collsCtrl.activeSection;
			}
		});
		if($("body").hasClass("page-help")){
			var offsetAmt = collsCtrl.lastScrollY - (collsCtrl.footerOffset-collsCtrl.winH);
			if(offsetAmt > 35){ offsetAmt = 35; }
			if(offsetAmt <= 0){
				$(".chips-credit").css({"transform":"translateY(0px)"});
			} else {
				$(".chips-credit").css({"transform":"translateY(-"+offsetAmt+"px)"});
			}
		}

		var footerThreshold = (collsCtrl.winH*1.04) * (collsCtrl.scrollPositions.length - 1) + collsCtrl.winH*0.04;
		if(scrollAmt >= footerThreshold && footerThreshold > 0){
			$("footer").addClass("zHigh");
		} else {
			$("footer").removeClass("zHigh");
		}


		collsCtrl.ticking = false;
	};
	// window.addEventListener('scroll', collsCtrl.onScroll, false);
	$("#collections").scroll(function(){
		collsCtrl.onScroll();
	});
	window.addEventListener('resize', collsCtrl.onResize, false);

	this.setURL = function(){
		window.location.hash = '#' + collsCtrl.activeSection;
	};
}]);

App.controller('collectionController',['$scope','pages','$timeout', function($scope,pages,$timeout){
	
}]);
App.controller('subNavController',['$scope','pages', function($scope,pages){
	$scope.pages = pages;
}]);
App.controller('staticController',['$scope','pages','$timeout', function($scope,pages,$timeout){
	staticCtrl = this;
	staticCtrl.scrollPositions=[];
	staticCtrl.lastScrollY = 0;
	staticCtrl.activeSection = '';
	$scope.pages = pages;

	$timeout(function() {
		staticCtrl.lastScrollY = window.scrollY;
		staticCtrl.onResize();
	});

	this.onResize = function() {
		this.footerOffset = $("footer").offset().top;
		this.winH = $(window).height();
		staticCtrl.scrollPositions=[];
		$timeout(function() {
			angular.forEach($('section'), function(item, i) {
				staticCtrl.scrollPositions[i] = [item.offsetTop, $(item).attr("class")];
			});

			staticCtrl.triggerScroll();
		}, 1000);
		$(".static section").last().css({"min-height":$(window).height() - 330});
	};
	this.onScroll = function() {
		staticCtrl.lastScrollY = window.scrollY;
		staticCtrl.requestTick();
	};

	this.requestTick = function() {
		if(!staticCtrl.ticking) {
			staticCtrl.ticking = true;
			requestAnimationFrame(staticCtrl.triggerScroll);
		}
	};
	this.footerOffset = 0;
	this.winH = 2000;
	this.triggerScroll = function(){
		var newSection = "";
		var scrollAmt = staticCtrl.lastScrollY;
		angular.forEach(staticCtrl.scrollPositions, function(item, i) {
			var next = i+1;
			var prev = i-1;
			if (i == staticCtrl.scrollPositions.length - 1) {
				if(scrollAmt + 150 > staticCtrl.scrollPositions[i][0]) {
					newSection = item[1];
				}
			} else {
				if(scrollAmt + 150 > staticCtrl.scrollPositions[i][0] && scrollAmt + 150 <= staticCtrl.scrollPositions[next][0]) {
					newSection = item[1];
				}
			}
		});
		$timeout(function() {
			if (staticCtrl.activeSection  !== newSection) {
				staticCtrl.activeSection = newSection;
				// window.location.hash = '#' + staticCtrl.activeSection;
				pages.activeSection = staticCtrl.activeSection;
			}
		});
		if($("body").hasClass("page-help")){
			var offsetAmt = staticCtrl.lastScrollY - (staticCtrl.footerOffset-staticCtrl.winH);
			if(offsetAmt > 35){ offsetAmt = 35; }
			if(offsetAmt <= 0){
				$(".chips-credit").css({"transform":"translateY(0px)"});
			} else {
				$(".chips-credit").css({"transform":"translateY(-"+offsetAmt+"px)"});
			}
		}
		staticCtrl.ticking = false;
	};
	window.addEventListener('scroll', staticCtrl.onScroll, false);
	window.addEventListener('resize', staticCtrl.onResize, false);
}]);
App.controller('imgController',['$scope','pages','$timeout', function($scope,pages,$timeout){
	// var localThis = this;
	// $scope.localData = null;
	// localThis.productInfo = "";
	// this.setLocalData = function(passedData){
	// 	$scope.localData = passedData;
	// };

	// this.getProductInfo = function(productID){
	// 	$.getJSON(storeURL+'/products/' + productID + '.json?callback=?').done(
	// 		function(x){
	// 			localThis.productInfo += '<div class="product-preview clearfix"><img src="' + x.product.image.src + '"><div class="prod-info"><h1><a href="' + x.product.url + '">' + x.product.title + '</a></h1><small>$' + x.product.variants[0].price + '</small><div id="button-add"><div class="button">Add to Bag</div></div></div>';
	// 		}
	// 	);
	// };

	// $scope.productInfo = function(passedData){
	// 	$scope.localData = passedData;
	// 	var productList = passedData;
	// 	for(var i=0;i<productList.length;i++){
	// 		localThis.getProductInfo(productList[i].product_id);
	// 		// console.log(productList[i]);
	// 	}
	// };
}]);

App.factory('pages', function($http,$q,$timeout){
	var title = "";
	var activeSection = '';
	return {
		activeSection: activeSection,
		getCollImages: function(passedCollection) {
			return $http.get('/shopify/?dType=collections&coll='+passedCollection,{'cache':true});
		},
		setTitle: function(newTitle) { title = newTitle; },
		setURL: function(){
			// console.log("setting url to " + this.activeSection);
			// $timeout(function(){
			// 	window.location.hash = '#' + activeSection;
			// });
			// $location.hash(activeSection);
		}
	};
});
