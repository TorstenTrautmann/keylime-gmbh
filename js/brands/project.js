var storeURL = '//shop.karastore.com';
(function ($) {
	$(".footer-close, #main").on("click",function(){
		$(".footer-overlay").hide();
	});
	$(".footer-toggle").on("click",function(e){
		e.preventDefault();
		var targetDiv = $(this).attr("rel");
		$(".footer-" + targetDiv).show();
		if(targetDiv === "mailinglist"){
			$(".inputter").focus();
		}
	});

	if($(".explore-page-grid").hasClass("filter-blog")){
		$(".single-item").each(function(){
			if(!$(this).hasClass("explore-type-blog")){
				$(this).remove();
			}
		});
	}

	$(document).ready(function(){
		getCartInfo();
		var prodID = '';
		function jsonpCallbackTwo(cartResponse){
			var finalMarkup = "";
			var totalPrice = 0;
			if(cartResponse.items.length > 0){
				for(var i = 0; i<cartResponse.items.length; i++){
					totalPrice += (cartResponse.items[i].price * cartResponse.items[i].quantity);
					var qtyText = "";
					if(cartResponse.items[i].quantity > 1){
						qtyText = " (×" + cartResponse.items[i].quantity + ")";
					}
					finalMarkup += '<li class="product"><span class="name">' + cartResponse.items[i].product_title + qtyText + '</span> <span class="price">$' + (cartResponse.items[i].price/100).toFixed(0) + '</span></li>';
				}
				finalMarkup += '<li class="subtotal"><span class="label">Subtotal</span> <span class="price">$'+(totalPrice/100).toFixed(0)+'</span></li>';
				$(".bag-products").html(finalMarkup);
				$(".my-bag, .my-bag-bg").addClass("bag-on");

				$(".count-0").removeClass("count-0");
				$(".cart-total").html(parseInt($(".cart-total").html()) + 1);
			}
		}
		function jsonpCallback(cartResponse){
			var finalMarkup = "";
			var totalPrice = 0;
			var buildString = '';
			if(cartResponse.items.length > 0){
				for(var i = 0; i<cartResponse.items.length; i++){
					buildString += cartResponse.items[i].variant_id +':'+cartResponse.items[i].quantity+',';
				}
			}
			buildString += prodID+':1';
			// console.log('//shop.karastore.com/cart/' + buildString + '');
			//shop.karastore.com/cart/add?id=7238231812&quantity=1
			//shop.karastore.com/cart/7238231812:1,15210826436:1
			// 15211303492
			// $('#button-add .button').append('<iframe src="//shop.karastore.com/cart/' + buildString + '" name="cart-frame"></iframe>');
			$('#button-add .button').append('<iframe src="//shop.karastore.com/cart/add?id=' + prodID + '&quantity=1" name="cart-frame"></iframe>');

			setTimeout(function(){
				$.ajax({
					url: "//shop.karastore.com/cart.json",
					dataType: 'jsonp',
					type: 'post',
					jsonpCallback: 'jsonpCallbackTwo',
					success: jsonpCallbackTwo,
				});
			}, 500);
	    }
		$(".api-request").on("click",function(){
			var productID = $(this).attr("data-id");
			// console.log('Getting product info for: ' + productID);
			getProductInfo($(this).parent(),productID);
		});

		$(".info-toggle").on("click",function(){
			if(!$(this).closest(".item-content").hasClass("data-loaded")){
				var products = $(this).attr("data-products");
				var productList = products.split("|");
				for(var i=0;i<productList.length;i++){
					getProductInfo($(this).parent(),productList[i]);
					// console.log(productList[i]);
				}
				$(this).closest(".item-content").addClass("data-loaded");
			}
			$(this).closest(".image-item").toggleClass("show-info");
		});

		$(".mobile-toggle").on("click",function(){
			if(mobileImgCount > 0 && !$("body").hasClass("menu-on")){
				var newRandomImg = Math.floor(Math.random() * mobileImgCount);
				$(".goodshoppe-wrap").css({"background-image":"url(" + mobileImgList[newRandomImg] + ")"});
				$("#goodshoppe-overlay").removeClass().addClass("pos-" + Math.floor(Math.random() * 10));
			}

			$("body").toggleClass("menu-on");
		});

		var zUp = 1;
		$(".img-steps li").hover(function(){
			$(this).css("z-index", zUp);
		},function(){
			zUp++;
		});

		$(".collection-item .sub-menu li a").click(function(e){
			e.preventDefault();
			var scrTarg = $(this).attr("data-scroll");
			var scrOff = $(scrTarg).offset().top;
			var parPos = $("#collections").scrollTop();
			
			window.location.hash = scrTarg;

			$("#collections").animate({
				scrollTop:parPos + scrOff
			},300);
		});

		$(".disablinkin").on("click",function(e){
			e.preventDefault();
		});
		$('html').on("click", '#button-add .button', function(e) {
			prodID = $(this).attr("data-id");
			// prodID = e.currentTarget.attributes[1].value;
			$.ajax({
				url: "//shop.karastore.com/cart.json",
				dataType: 'jsonp',
				type: 'post',
				jsonpCallback: 'jsonpCallback',
				success: jsonpCallback,
			});	
			// $(this).append('<iframe src="//karastore.com/cart/'+prodID+':1" name="cart-frame"></iframe>');
			// $.ajax({
		 //        url: "//karastore.com/cart.json",
		 //       dataType: 'jsonp',
		 //       type: 'post',
		 //       data: "id="+prodID+",quantity=1",
			// 	jsonpCallback: 'jsonpCallback',
			// 	success: jsonpCallback
			//  });  

		});

		loadMobileMenuImages();
	});
	
	function getCartInfo(){
		$.getJSON(storeURL+'/cart.json?callback=?').done(
			function(x){
				$(".cart-total").removeClass().addClass("cart-total count-" + x.item_count).html(x.item_count);
			}
		);

	}

	function getProductInfo(containObj,passedURL){
		$.getJSON(storeURL+'/products/' + passedURL + '.json?callback=?').done(
			function(x){
				// console.log(x.product.url);
				containObj.find(".item-info-inner").append('<div class="product-preview clearfix"><img src="' + x.product.image.src + '"><div class="prod-info"><h1><a href="' + x.product.url + '">' + x.product.title + '</a></h1><small>$' + x.product.variants[0].price + '</small></div>');
			}
		);

	}

	var mobileImgCount = 0;
	var mobileImgList = null;
	function loadMobileMenuImages(){
		$.ajax({
			url: "/shopify/?dType=mobileImages",
			dataType: 'json',
			success: function(results){
				mobileImgList = results.mobileImages;
				mobileImgCount = mobileImgList.length;
				// console.log(mobileImgList);
				// console.log(mobileImgCount);
			},
		});	
	}

	$(".close-bag, .bg-close").on("click",function(){
		$(".bag-on").removeClass("bag-on");
	});

	$(".blogMore").hover(function(){
		$(this).addClass("blogHover");
	},function(){
		$(this).removeClass("blogHover");
	});

	if($(".about-body").hasClass("about-body")){
		var newZindex = 3;
		var selected = null, // Object of the element to be moved
		    x_pos = 0, y_pos = 0, // Stores x & y coordinates of the mouse pointer
		    x_elem = 0, y_elem = 0; // Stores top, left values (edge) of the element

		// Will be called when user starts dragging an element
		function _drag_init(elem) {
			// Store the object of the element which needs to be moved
			selected = elem;
			x_elem = x_pos - selected.offsetLeft;
			y_elem = y_pos - selected.offsetTop;
		}

		// Will be called when user dragging an element
		function _move_elem(e) {
			x_pos = document.all ? window.event.clientX : e.pageX;
			y_pos = document.all ? window.event.clientY : e.pageY;
			if (selected !== null) {
				selected.style.left = (x_pos - x_elem) + 'px';
				selected.style.top = (y_pos - y_elem) + 'px';
				selected.style.zIndex = newZindex;
			}
		}

		// Destroy the object when we are done
		function _destroy() {
			selected = null;
		}

		// Bind the functions...
		var targetEls = document.getElementsByClassName("draggable");
		for (var i = 0; i < targetEls.length; i++) {
			targetEls[i].onmousedown = function () {
				newZindex++;
				_drag_init(this);
				return false;
			};
		}
		

		document.onmousemove = _move_elem;
		document.onmouseup = _destroy;
	}
})(jQuery);
