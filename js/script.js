
var sections = {
	welcome: {

				navigationItem: '',
				navigationHash: '',
				navigationColor: 'ffffff',
				contentHeight: {desktop: 900, tabletPortrait: 800, mobileLandscape: 400, mobilePortrait: 604},
				navUnderline: {

					desktop: 			{height: 0, left: 200, width: 39},
					tabletPortrait: 	{height: 0, left: 380, width: 49},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				},
				keyframes: [

					{
						startPercentage: 60,
						action: showWorkCards
					}

				]

			},

	work: {

				navigationItem: 'Featured Work',
				navigationHash: 'work',
				navigationColor: '000000',
				contentHeight: {desktop: 3000, tabletPortrait: 2400, mobileLandscape: 3760, mobilePortrait: 7130},
				//contentHeight: {desktop: 1300, tabletPortrait: 1150, mobileLandscape: 3150, mobilePortrait: 3150},
				navUnderline: {

					desktop: 			{height: 4, left: 445,	width: 41},
					tabletPortrait: 	{height: 4, left: 341,	width: 41},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				},
				keyframes: [

					{
						startPercentage: 0,
						action: showWorkCards
					}

				]
	},

	pastwork: {

				navigationItem: 'Past Work',
				navigationHash: 'pastwork',
				navigationColor: '000000',
				contentHeight: {desktop: 900, tabletPortrait: 750, mobileLandscape: 3760, mobilePortrait: 1975},
				//contentHeight: {desktop: 1300, tabletPortrait: 1150, mobileLandscape: 3150, mobilePortrait: 3150},
				navUnderline: {

					desktop: 			{height: 4, left: 534,	width: 82},
					tabletPortrait: 	{height: 4, left: 428,	width: 82},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				},
	},

	about:   {

				navigationItem: 'About',
				navigationHash: 'about',
			 	navigationColor: 'ffffff',
			 	contentHeight: {desktop: 840, tabletPortrait: 1100, mobileLandscape: 850, mobilePortrait: 900},
			 	navUnderline: {

					desktop: 			{height: 4, left: 661,	width: 50},
					tabletPortrait: 	{height: 4, left: 428,	width: 50},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				}

			 },


	approach: {

				navigationItem: 'Approach',
				navigationHash: 'approach',
				navigationColor: 'ffffff',
				contentHeight: {desktop: 900, tabletPortrait: 900, mobileLandscape: 1365, mobilePortrait: 1370},
				navUnderline: {
					desktop: 			{height: 4, left: 754, width: 78},
					tabletPortrait: 	{height: 4, left: 523, width: 78},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}
				},
				keyframes: [

					{
						startPercentage: 0,
						action: showApproachColumns

					}

				]
	},


	rules: {

				navigationItem: 'Rules',
				navigationHash: 'rules',
				navigationColor: 'ffffff',
				contentHeight: {desktop: 860, tabletPortrait: 1000, mobileLandscape: 820, mobilePortrait: 770},
				navUnderline: {
					desktop: 			{height: 4, left: 878,	width: 79 },
					tabletPortrait: 	{height: 4, left: 647,	width: 79 },
					mobileLandscape: 	{height: 0, left: 0,	width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}
				},
				keyframes: [

					{
						startPercentage: 0,
						action: showRulesColumns
					}

				]

	 },



	weare: {

				navigationItem: 'We are',
				navigationHash: 'weare',
				navigationColor: 'ffffff',
				contentHeight: {desktop: 900, tabletPortrait: 900, mobileLandscape: 400, mobilePortrait: 600},
				navUnderline: {

					desktop: 			{height: 4, left: 1003, width: 55},
					tabletPortrait: 	{height: 4, left: 772, width: 55},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				}
	 },


	connect: {

				navigationItem: 'Connect',
				navigationHash: 'connect',
				navigationColor: '000000',
				contentHeight: {desktop: 'stage', tabletPortrait: 'stage', mobileLandscape: 'stage', mobilePortrait: 'stage'},
				navUnderline: {

					desktop: 			{height: 4, left: 1104 , width: 66},
					tabletPortrait: 	{height: 4, left: 873, width: 66},
					mobileLandscape: 	{height: 0, left: 0, width: 0},
					mobilePortrait: 	{height: 0, left: 0, width: 0}

				},
	 		}

};


// when selecting a work card, we need to scroll vertically so the top card edge is visible, different responsivelly. Scroll position will be calculated:
// scrollpos = worksection - offset, so

var workCardsLoadingOffsets = {

	desktop: 			{workCardI30: 220, workCardFrontier: 220, workCardMDS: 220},
	tabletPortrait: 	{workCardI30: 50, workCardFrontier: 50, workCardMDS: 50},
	mobileLandscape: 	{workCardI30: 670, workCardFrontier: 1040, workCardMDS: 300},
	mobilePortrait: 	{workCardI30: 670, workCardFrontier: 1040, workCardMDS: 300}

}

var responsivenessOn = false;
var responsiveModesStartWidths = {mobilePortrait: 0, mobileLandscape: 480, tabletPortrait: 768, desktop: 1200};
var currentResponsiveMode = 'desktop';
var currentMapMode 		  = 'desktop';

var sectionsArray = $.map(sections, function(k, v) { return [k]; });
var currentSection = null;

var selectedFeaturedWorkCurrentSlide = 0;
var selectedFeaturedWorkSlidesNum = 0;
var $selectedFeaturedWorkCurrentSlide;
var selectedFeaturedWork = '';
var isSelectedFeaturedWorkInitialSlide = false;
var isSlideAnimationBusy = false;
var $slideToDestroy;
var zIndexCounter = 10;

var currentNavigationItem = '';
var currentNavigationHash = '';

var bgTransitionDuration = {desktop: 600, tabletPortrait: 800, mobileLandscape: 200, mobilePortrait: 400};

var bgScrollCurrent = 0;
var bgScrollTarget = 0;

var scrollCurrent = 0;
var scrollTarget = 0;
var scrollEasing = 0.1;
var bgScrollEasing = 0.2;

var scrollSave = 0;
var scrollDirection = 1;

var handleOffset = 0;

var scrollingEnbaled = false;
var scrollPercentSection = 0;
var scrollPercentGlobal = 0;
var isScrolling = false;
var scrollerHeight = 0;
var scrollThumbHeight = 115;
var scrollThumbPos = 0;

var scrollTouchPos = {x:0, y:0};
var scrollTouchStartPos = 0;

var navigationChangeOffset = 300;

var stageWidth, stageHeight;

var $scrollThumb;
var $header;
var $footer;
var $socialButtons;
var $menuHoverUnderline;
var $googleMap;
var $workSlidesHolder;
var $selectedFeaturedWorkTpl;

var maxScroll = 0;
var maxBgScroll = 0;

var googleMap;
var mapInitComplete = false;
var spaceMaker;
var contentInfoBox;
var isMapCollapsed = false;

var connectWindowOffset = 50;
var connectWindowHeight = 365;
var connectWindowWidth = 460;

var mapPopupTemplate;
var featuredWorkTemplates = [];

var currentManifestFilesToLoad;
var currentManifestFilesLoaded;

var shapesPreloader;


var $loadedTweetsList;
var currentTweetIndex = 0;
var changeTweetInterval;

var loadedImages = [];
var preloaderManifests = {

	main: {

		urls: [
			"img/bg-welcome.jpg",
			"img/bg-about.jpg",
			"img/bg-approach.jpg",
			"img/bg-rules.jpg",
			"img/bg-weare.jpg"
		]
	} /*,

	workCardMDS: {

		isLoaded : false,
		urls: [
			"img/work-mds-slide1-bg.jpg",
			"img/work-mds-slide1-logo.png",
			"img/work-mds-slide2-bg.jpg",
			"img/work-mds-slide2-image.png",
			"img/work-mds-slide3-image.jpg",
			"img/work-mds-slide4-image.png",
			"img/work-mds-slide5-bg.jpg"
		]

	},

	workCardI30: {

		isLoaded : false,
		urls: [
			"img/work-i30-slide1-bg.jpg",
			"img/work-i30-slide2-bg.jpg",
			"img/work-mds-slide2-bg.jpg",
			"img/work-i30-slide3-image.png",
			"img/work-i30-slide4-bg.jpg",
			"img/work-i30-slide4-image.png",
			"img/work-i30-slide5-bg.jpg"
		]

	},

	workCardFrontier: {

		isLoaded : false,
		urls: [
			"img/work-frontier-slide1-bg.jpg",
			"img/work-frontier-slide2-image.png",
			"img/work-mds-slide2-bg.jpg",
			"img/work-frontier-slide3-bg.jpg",
			"img/work-frontier-slide3-image.png",
			"img/work-frontier-slide4-image.png",
			"img/work-frontier-slide5-bg.jpg",
			"img/work-frontier-slide5-image.png",
			"img/work-frontier-slide6-bg.jpg"
		]

	}
*/

};

/* jQuery .browser added for 1.9.1 from migration plugin */

$.uaMatch = function( ua ) {
	ua = ua.toLowerCase();

	var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		/(msie) ([\w.]+)/.exec( ua ) ||
		ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		[];

	return {
		browser: match[ 1 ] || "",
		version: match[ 2 ] || "0"
		};
};

if ( !$.browser ) {
	matched = $.uaMatch( navigator.userAgent );
	browser = {};

	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}

	// Chrome is Webkit, but Webkit is also Safari.
	if ( browser.chrome ) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}

	$.browser = browser;
}

var isMoveWheel = false;
var isTouch = ('ontouchstart'in window);
var isIPad = (function(){

	var uagent = navigator.userAgent.toLowerCase();

	if(uagent.search("ipad") >-1 &&uagent.search("webkit") >- 1)
		return true;
	else return false;

})();

var isScrollbar = !isIPad;

var isCSSAni = (Modernizr.csstransitions || isTouch);
var isCSSTrans = Modernizr.csstransforms3d;
var cssHead = "";

if($.browser.webkit) {
	cssHead = "-webkit-";
} else if($.browser.msie) {
	cssHead = "-ms-";
}else if($.browser.mozilla) {
	cssHead = "-moz-";
	isCSSAni = false;}
else if($.browser.opera) {
	cssHead = "-o-";
	isCSSAni = false;
}

var cssHeadTransform = cssHead + "transform";

/***************************************

	App Entry point

****************************************/

$(document).ready(function() {

	$header 		= $('#header');
	$footer 		= $('#footer');
	$scrollThumb 	= $('#scrollThumb');
	$socialButtons 	= $('.socialLinks li a');
	$googleMap 		= $('#googleMap');
	$workSlidesHolder = $('#workSlidesHolder');

	if(isTouch) {

		$('html').addClass('touchDevice');

		// just show everything for touch as it doesn't work smooth and nice
		showWorkCards();
		showApproachColumns();
		showRulesColumns();


	} else {

		$('html').addClass('noTouchDevice');

	}

	stageWidth = $(window).width();
	stageHeight = $(window).height();

	// init the fullscreen elements
	resizeHandler();
	$(window).resize(resizeHandler);

	// ipad resize on orientation change workaroud
	$(window).bind('orientationchange', function(){ setTimeout(resizeHandler, 1000); });

	$(window).scrollTop(0);

	// start the preloader
	initPreloader();

	// expect klicks on featured work tiles
	//$('.workCard').click(workCardClickHandler);

	// compile Handlebars templates
    /*
	featuredWorkTemplates['workCardMDS'] 	  = Handlebars.compile($("#workCardMDSTemplate").html());
	featuredWorkTemplates['workCardFrontier'] = Handlebars.compile($("#workCardFrontierTemplate").html());
	featuredWorkTemplates['workCardI30'] 	  = Handlebars.compile($("#workCardI30Template").html());
    */

});


function DummyPreloader(preloaderCompleteHandler) {
    this.preloaderCompleteHandler = preloaderCompleteHandler;
}

DummyPreloader.prototype = {

    constructor: DummyPreloader,
    preloaderCompleteHandler: undefined,
    progress: 0,

    updateProgress: function(progress) {

        if (this.progress >= 1) {
            return;
        }

        this.progress = progress;
        if (this.progress >= 1) {
            this.preloaderCompleteHandler();
        }
    },

    destroy: function() { }
};



function initPreloader() {

	currentManifestfilesToLoad = preloaderManifests.main.urls.length;
	currentManifestfilesLoaded = 0;

	var preloader = new createjs.LoadQueue();

	preloader.addEventListener("fileload", 		fileloadHandler);
	preloader.setMaxConnections(1);
	preloader.loadManifest(preloaderManifests.main.urls);


    if (jQuery('html').hasClass('canvas')) {
	    shapesPreloader = new ShapesPreloader(preloaderCompleteHandler);
    }
    else {
        shapesPreloader = new DummyPreloader(preloaderCompleteHandler);
    }

	$('#curtain').remove();

}

/* The main preloader stripe */
function fileloadHandler(event) {

	var img = event.result;
	loadedImages[event.item.src] = img;

	currentManifestfilesLoaded++;

	var progress = currentManifestfilesLoaded/currentManifestfilesToLoad;
	shapesPreloader.updateProgress(progress);

	$('#backgroundWrapper .scrollingArea div[data-stretch="'+event.item.src+'"]').anystretch(img);

}

/* Start the app, main assets are ready */
function preloaderCompleteHandler() {

	shapesPreloader.destroy();

	initMenu();

	window.requestAnimationFrame(render);

	initGoogleMap();
	//initTwitterFeed();

	// if user is coming with a hash link, move to that one. If has no hash, show "view works" button
	handleHashLink();

}

function enableScrolling() {

	scrollingEnbaled = true;

	$scrollThumb.bind("mousedown", startScroll);

	if (isTouch) {
		$(document)[0].addEventListener('touchstart', touchStartHandler, true);
		$(document)[0].addEventListener('touchmove', touchMoveHandler, true);
	} else {
		$(document).bind('mousewheel', wheelHandler);
		showScrollbar();
	}

}

function disableScrolling() {

	scrollingEnbaled = false;

	$scrollThumb.unbind("mousedown");

	if (isTouch) {
		$(document)[0].removeEventListener('touchstart');
		$(document)[0].removeEventListener('touchmove');
	} else {
		$(document).unbind('mousewheel');
		hideScrollbar();
	}

}

function showScrollbar() {

	$('#scroller').css('display', 'block');

	TweenLite.killTweensOf($('#scroller'));
	TweenLite.to($('#scroller'), 0.5, {css:{'right': '0px'}, ease:Power2.easeOut});

}


function hideScrollbar() {

	TweenLite.killTweensOf($('#scroller'));
	TweenLite.to($('#scroller'), 0.5, {css:{'right': '-10px'}, ease:Power2.easeOut});
}

function resizeHandler() {

	stageWidth = $(window).width();
	stageHeight = $(window).height();

	var oldResponsiveMode = currentResponsiveMode;

 	_.each(responsiveModesStartWidths, function(width, mode) {

	if(stageWidth >= width)
		currentResponsiveMode = mode;
	}, this);

	if(oldResponsiveMode != currentResponsiveMode) {
		changeNavigation(currentSection, true);
	}

	if(currentResponsiveMode == 'desktop' || currentResponsiveMode == 'tabletPortrait') {
		$('.mobileMenu').hide();
	}

	$('#contentWrapper').width(stageWidth).height(stageHeight - $header.height());
	$('#backgroundWrapper, .anystretch, .fullscreen').width(stageWidth).height(stageHeight);

	$('#connect').height(stageHeight);
	$googleMap.height(stageHeight - $footer.height());

	maxScroll = recalculateHeights();
	scrollerHeight = stageHeight;

	$('#scroller').height(scrollerHeight);
	setScrollBarPosition(scrollPercentGlobal);

	scrollEasing = 1;
	bgScrollEasing = 1;

	scrollTarget = (maxScroll*scrollPercentGlobal);

	var newMapMode = 'desktop';

	if((stageHeight <= connectWindowHeight + $header.height() + $footer.height() + connectWindowOffset) || stageWidth <= connectWindowWidth ) {
		newMapMode = 'mobile';
	} else {
		newMapMode = 'desktop';
	}

	if(currentMapMode != newMapMode )  {

		currentMapMode = newMapMode;

		if(contentInfoBox && mapInitComplete) {
			openInfoBox();
		}

	}

	if(contentInfoBox) contentInfoBox.panMap();

}

function recalculateHeights() {

	var currentOffset = 0;
	var sectionHeight = 0;

	_.each(sections, function(section) {

		section.startOffset =  currentOffset;

		if(section.contentHeight[currentResponsiveMode] == 'stage')
			sectionHeight = stageHeight;
		else
			sectionHeight = section.contentHeight[currentResponsiveMode];

		currentOffset 		+= sectionHeight;
		section.endOffset 	=  currentOffset;

	}, this);

	// returns the total height
	maxBgScroll = stageHeight * (sectionsArray.length-1);

	return currentOffset - stageHeight;

}

// Main render cycle, runs constantly
function render() {

	window.requestAnimationFrame(render);

	// main scrolling handling for both bg layer and content layer
	if(isCSSTrans) {

		$('#contentWrapper .scrollingArea').css(cssHeadTransform, 'translate3d(0px, '+ (-scrollCurrent) +'px, 0px)');
		$('#backgroundWrapper .scrollingArea').css(cssHeadTransform, 'translate3d(0px, '+ (-bgScrollCurrent) +'px, 0px)');
		$('#contentWrapper .scrollingArea').css('transform', 'translate3d(0px,'+ (-scrollCurrent) +'px, 0px)');
		$('#backgroundWrapper .scrollingArea').css('transform', 'translate3d(0px, '+ (-bgScrollCurrent) +'px, 0px)');
	} else {

		$('#contentWrapper .scrollingArea').css({'top': -scrollCurrent +'px'});
		$('#backgroundWrapper .scrollingArea').css({'top': -bgScrollCurrent +'px'});
	}


	// easing calculation for content
	if (Math.ceil(scrollCurrent) !== Math.floor(scrollTarget)) {

            scrollCurrent += (scrollTarget - scrollCurrent) * scrollEasing;
            scrollDirection = scrollTarget > scrollCurrent ? 1 : -1;

    } else {

    	scrollCurrent = Math.ceil(scrollCurrent);
    }

    // easing calculation for bg
    if (Math.ceil(bgScrollCurrent) !== Math.floor(bgScrollTarget)) {

    		bgScrollCurrent += (bgScrollTarget - bgScrollCurrent) * bgScrollEasing;
    } else {

    	bgScrollCurrent = Math.ceil(bgScrollCurrent);

    }

    // scrollbar update
    setScrollBarPosition(scrollCurrent / maxScroll);
    scrollPercentGlobal = scrollThumbPos/(scrollerHeight-scrollThumbHeight);

    // looking for a "current" content section
    currentSection = null;
   	var currentSectionIndex = 0;

   	var navigationSection = null;

    _.each(sections, function(section) {


    	// navigation has a bit different "current" selection in between the sections
    	if((scrollCurrent + navigationChangeOffset) >= section.startOffset && (scrollCurrent + navigationChangeOffset) < section.endOffset) {

    		navigationSection = section;

    	}

    	// return if we've found one already
    	if(currentSection) return;

			if(scrollCurrent >= section.startOffset && scrollCurrent < section.endOffset) {

    			currentSection = section;
				return;
    		}

    	currentSectionIndex++;

	}, this);

    // update navigation
	changeNavigation(navigationSection);

	// section progress/keyframes (triggers keyframe actions)
	var scrollPercentSection = Math.round((currentSection.startOffset - scrollCurrent) / (currentSection.startOffset - currentSection.endOffset)*100);

	if(currentSection.keyframes && currentSection.keyframes.length) {

		_.each(currentSection.keyframes, function(keyframe) {

			if(scrollPercentSection >= keyframe.startPercentage) {

				if(!keyframe.isActivated) {
					keyframe.action();
					keyframe.isActivated = true;
				}

			}

		});

	}

	// background doesn't constatly move, this part handles when and how
    var bgTransitionStart = currentSection.endOffset - bgTransitionDuration[currentResponsiveMode];

    var bgProgress = (bgTransitionStart - scrollTarget) / (bgTransitionStart - currentSection.endOffset);
	bgProgress = Math.max(0, Math.min(1, bgProgress));

	bgScrollTarget = stageHeight * currentSectionIndex + Math.round(stageHeight*bgProgress);

	if(bgScrollTarget > maxBgScroll)
		bgScrollTarget = maxBgScroll;

	// disable content for map access hack
    if(scrollCurrent >= sectionsArray[sectionsArray.length-1].startOffset-stageHeight/4) {
	   	$('#contentWrapper').css('display', 'none');
    } else {
       	$('#contentWrapper').css('display', 'block');
    }


}

function changeNavigation(section, resizeFlag) {

	if(!section) return;

	var newNavitaionItem   = section.navigationItem;
	var newNavitaionHash   = section.navigationHash;

	if(newNavitaionItem != currentNavigationItem || resizeFlag) {

		var underlineTargetWidth 	= section.navUnderline[currentResponsiveMode].width;
		var underlineTargetLeft  	= section.navUnderline[currentResponsiveMode].left;
		var underlineTargetHeight	= section.navUnderline[currentResponsiveMode].height;

		TweenLite.killTweensOf($('#menuUnderline'));
		TweenLite.to($('#menuUnderline'), 0.5, {css:{'height': underlineTargetHeight + 'px', 'left': underlineTargetLeft + 'px', 'width': underlineTargetWidth + 'px' }, ease:Power2.easeOut});

		currentNavigationItem = newNavitaionItem;
		currentNavigationHash = newNavitaionHash;

		if(!isTouch)
			window.location.hash = '#' + currentNavigationHash;

	}
}

function scrollUpdate(event){


	scrollThumbPos = event.pageY + handleOffset;
	scrollThumbPos = Math.max(0,Math.min(scrollerHeight-scrollThumbHeight,scrollThumbPos));
	scrollPercentGlobal = scrollThumbPos/(scrollerHeight-scrollThumbHeight);
	scrollPercentGlobal = Math.max(0,Math.min(1,scrollPercentGlobal));

	scrollTarget = (maxScroll*scrollPercentGlobal);


	return false;
}

function setScrollBarPosition(posPerecents){


	if(posPerecents > 1)
		posPerecents = 1;

	scrollThumbPos = (scrollerHeight-scrollThumbHeight)*posPerecents;
	$scrollThumb.css("top",scrollThumbPos);
}

function startScroll(event) {

	scrollEasing = 0.08;
	bgScrollEasing = 0.1;

    isScrolling = true;

    handleOffset = scrollThumbPos - event.pageY;

    $(document).on("mousemove", scrollUpdate);
    $(document).on("mouseup", endScroll);

    return false;
}

function endScroll(event){

	isScrolling = false;

	$(document).off("mousemove",scrollUpdate);
	$(document).off("mouseup",endScroll);

	return false;
}

function touchStartHandler(e) {

	if(!scrollingEnbaled) return;

	var mx = e.touches[0].pageX;
    var my = e.touches[0].pageY;

    scrollTouchPos.y = my;

   	scrollEasing = 0.3;
	bgScrollEasing = 0.4;

    scrollTouchStartPos = scrollCurrent;
}

function touchMoveHandler(e) {

	e.preventDefault();

	if(!scrollingEnbaled) return;

	var mx = e.touches[0].pageX;
    var my = e.touches[0].pageY;

    var offset = (scrollTouchPos.y - my);
    scrollTarget = Math.max(0, scrollTouchStartPos + offset);
    checkScrollValue();
}

function checkScrollValue() {

    isMoveWheel = true;

    if (scrollTarget < 0) {
    	scrollTarget = 0;
    } else if (scrollTarget > maxScroll) {
    	scrollTarget = maxScroll;
    }

}

function wheelHandler(e, delta, deltaX, deltaY) {

	scrollEasing = 0.05;
	bgScrollEasing = 0.1;

    scrollTarget -= delta * 50;
    checkScrollValue();

}

function menuClickHandler(event) {

	event.preventDefault();

	if($(this).hasClass('mobile'))
		$('.mobileMenu').slideToggle();

	scrollEasing = 0.2;
	bgScrollEasing = 0.3;

	var searchFor = $(this).attr('href');

    if ('#' != searchFor.substr(0, 1)) {
        window.location = searchFor;
        return false;
    }


	var section = _.find(sections, function(section, key){ return '#'+key == searchFor; });

	if(!section) return false;

	scrollTarget = (section == sections.welcome ? 0 : section.startOffset);

	return false;

}

function handleHashLink() {

	// if no hash, just enable scrolling and leave
	if(!window.location.hash) {

		enableScrolling();
		return;
	}

	var searchFor = window.location.hash.toLowerCase();

	/*var isFound = false;
	// search for work card first
	$('.workCard').each(function(){

		if('#' + $(this).attr('data-hashlink') == searchFor) {
			$(this).click();

			// if it's a work card, we don't need to enable scrollbar, so just return
			isFound = true;
		}

	});

	if(isFound)
		return;*/

	// try to find a section to scroll to
	var section = _.find(sections, function(section, key){ return '#'+section.navigationHash == searchFor; });

	if(!section)
		return;

	enableScrolling();

	scrollEasing = 0.2;
	bgScrollEasing = 0.3;

	scrollCurrent = scrollTarget = (section == sections.welcome ? 0 : section.startOffset);

}


/* Request Animation Frame */
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

function initMenu() {

	TweenLite.to($('#header'), 1, {top: '0px', ease:Power2.easeOut});

	$('.menuItem, .logo').click(menuClickHandler);

	$('.mobileMenuButton').click(function(){

		$('.mobileMenu').slideToggle();
	});

}

/* Map */
function initGoogleMap() {

	mapInitComplete = true;

	var tplHtml = $("#mapPopupTemplate").html();

	mapPopupTemplate = Handlebars.compile(tplHtml);

	var styles =[
	  {
	    "stylers": [
	      { "saturation": -100 }
	    ]
	  },{
         featureType: "poi",
         elementType: "labels",
         stylers: [
           { visibility: "off" }
         ]
       }
	];

	var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});

	var draggable = true;

	if(isTouch) {
		draggable = false;
	}

	var mapOptions = {
	    zoom: 15,
	    maxZoom: 15,
	    minZoom: 15,
	    disableDoubleClickZoom: true,
	    center: new google.maps.LatLng(51.23404,6.17874),
	    mapTypeControlOptions: {
	      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
   		},
   		disableDefaultUI: true,
   		scrollwheel: false,
   		draggable: draggable,
   	};

  	googleMap = new google.maps.Map(document.getElementById('googleMap'),	mapOptions);

  	spaceMaker = new google.maps.Marker({
        position: new google.maps.LatLng(51.23490,6.17740),
        map: googleMap,
        icon : 'img/space-marker.png'
    });

    openInfoBox();


  	googleMap.mapTypes.set('map_style', styledMap);
  	googleMap.setMapTypeId('map_style');


}

function openInfoBox() {

	if(contentInfoBox) {
		contentInfoBox.remove();
		contentInfoBox = null;
	}

	google.maps.event.addListener(spaceMaker, "click", function(e) {
      contentInfoBox = new InfoBox({latlng: spaceMaker.getPosition(), map: googleMap});
    });


	google.maps.event.trigger(spaceMaker, "click");
    google.maps.event.clearListeners(spaceMaker, 'click');

}

function InfoBox(opts) {

	google.maps.OverlayView.call(this);
	this.latlng_ = opts.latlng;
	this.map_ = opts.map;

	this.offsetVertical_ = -211;
	this.offsetHorizontal_ = -216;

	this.height_ = 189;
	this.width_ = 430;

	this.heightMobile_ = 150;
	this.widthMobile_ = 320;

	this.offsetVerticalMobile_ = -172;
	this.offsetHorizontalMobile_ = -145;

	this.defaultMapCenter = new google.maps.LatLng(51.23490,6.17740);
	this.currentMode_ = 'desktop';

	this.setMap(this.map_);
}

InfoBox.prototype = new google.maps.OverlayView();

InfoBox.prototype.remove = function() {

	if (this.div_) {
		this.div_.parentNode.removeChild(this.div_);
		this.div_ = null;
	}

	this.setMap(null);
};

InfoBox.prototype.draw = function() {

 	this.createElement();

 	if (!this.div_) return;

	var pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng_);
	if (!pixPosition) return;

	if(this.currentMode_ == 'desktop' && !isMapCollapsed) {

		this.div_.style.width = this.width_ + "px";
		this.div_.style.left = (pixPosition.x + this.offsetHorizontal_) + "px";
		this.div_.style.height = this.height_ + "px";
		this.div_.style.top = (pixPosition.y + this.offsetVertical_) + "px";
		this.div_.style.display = 'block';

	} else {

		this.div_.style.width = this.widthMobile_ + "px";
		this.div_.style.left = (pixPosition.x + this.offsetHorizontalMobile_) + "px";
		this.div_.style.height = this.heightMobile_ + "px";
		this.div_.style.top = (pixPosition.y + this.offsetVerticalMobile_) + "px";
		this.div_.style.display = 'block';

	}

};

InfoBox.prototype.createElement = function() {

	var panes = this.getPanes();
	var div = this.div_;


	if (!div || this.currentMode_ != currentMapMode) {

		this.currentMode_ = currentMapMode;
		var $parsedResult = $($.parseHTML(mapPopupTemplate({
			isMobile: (this.currentMode_ == 'mobile' ? 'mobile' : ''),
			isCollapsed: ((this.currentMode_ == 'mobile' || isMapCollapsed) ? 'collapsed' : '')
		}))).filter('.contactMapPopup');

		// for some reason IE8 has an issue with parsing template and adds a text node as 0 element, so we need to remove first one
		div = this.div_ = $parsedResult.get(0);

		panes.overlayMouseTarget.appendChild(div);
		this.panMap();

		$('#mapCollapse').click(function() {
			isMapCollapsed = true;
			 openInfoBox();
		});

		$('#mapExpand').click(function() {
			isMapCollapsed = false;
			openInfoBox();
		});

		$('#mapSkypeButton').click(function(e){

			e.preventDefault();
			$('#skypeButton_paraElement').find('a').click();
			return false;

		});

	} else if (div.parentNode != panes.overlayMouseTarget) {

		div.parentNode.removeChild(div);
		panes.overlayMouseTarget.appendChild(div);

	}
}

/* Pan the map to fit the InfoBox.
 */
InfoBox.prototype.panMap = function() {

	// if we go beyond map, pan map
	var map = this.map_;

	var bounds = map.getBounds();
	if (!bounds) return;

	map.setCenter(spaceMaker.position);

	var mapHeightPxl = stageHeight - $header.height() - $footer.height();
	var targetPxl = mapHeightPxl/2 - (this.currentMode_ == 'desktop' ? this.height_/2 : this.heightMobile_/2);


	var targetLatLng = (this.getProjection().fromContainerPixelToLatLng(new google.maps.Point(stageWidth/2, targetPxl)));
	map.setCenter(targetLatLng);

	// Remove the listener after panning is complete.
	google.maps.event.removeListener(this.boundsChangedListener_);
	this.boundsChangedListener_ = null;

};



/* Featured works */

// Initial show cards (used in section keyframes)
function showWorkCards() {

/*
	if(!isTouch) {

		TweenLite.to($('#work-section #workCardMDS'), 1, {'autoAlpha':1});
		TweenLite.to($('#work-section #workCardI30'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#work-section #workCardFrontier'), 1, {'autoAlpha':1, delay: 0.7});

	} else {
		$('#work-section .workCard').css({'visibility': 'visible', 'opacity': 1});
	}
*/

	if(!isTouch) {
		TweenLite.to($('#work-section').find('.span3:nth-of-type(1)'), 1, {'autoAlpha':1});
		TweenLite.to($('#work-section').find('.span3:nth-of-type(2)'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#work-section').find('.span3:nth-of-type(3)'), 1, {'autoAlpha':1, delay: 0.7});
		TweenLite.to($('#work-section').find('.span3:nth-of-type(4)'), 1, {'autoAlpha':1, delay: 0.9});
	} else {
		$('#work-section .span3').css({'visibility': 'visible', 'opacity': 1});
	}

}

function showApproachColumns() {
/*
	if(!isTouch) {

		TweenLite.to($('#approach-section .col1'), 1, {'autoAlpha':1});
		TweenLite.to($('#approach-section .col2'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#approach-section .col3'), 1, {'autoAlpha':1, delay: 0.6});
		TweenLite.to($('#approach-section .col4'), 1, {'autoAlpha':1, delay: 0.9});
		TweenLite.to($('#approach-section .col5'), 1, {'autoAlpha':1, delay: 1.2});

	} else {
		$('#approach-section .column').css({'visibility': 'visible', 'opacity': 1});
	}
*/

	if(!isTouch) {

		TweenLite.to($('#approach-section').find('.span3:nth-of-type(1)'), 1, {'autoAlpha':1});
		TweenLite.to($('#approach-section').find('.span3:nth-of-type(2)'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#approach-section').find('.span3:nth-of-type(3)'), 1, {'autoAlpha':1, delay: 0.6});
		TweenLite.to($('#approach-section').find('.span3:nth-of-type(4)'), 1, {'autoAlpha':1, delay: 0.9});


	} else {
		$('#approach-section .span3').css({'visibility': 'visible', 'opacity': 1});
	}
}


function showRulesColumns() {
/*
	if(!isTouch) {

		TweenLite.to($('#services-section .col1'), 1, {'autoAlpha':1});
		TweenLite.to($('#services-section .col2'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#services-section .col3'), 1, {'autoAlpha':1, delay: 0.6});
		TweenLite.to($('#services-section .col4'), 1, {'autoAlpha':1, delay: 0.9});

	} else {
		$('#services-section .column').css({'visibility': 'visible', 'opacity': 1});
	}
*/

	if(!isTouch) {

		TweenLite.to($('#rules-section .span12 .row:first').find('.span4:nth-of-type(1)'), 1, {'autoAlpha':1});
		TweenLite.to($('#rules-section .span12 .row:first').find('.span4:nth-of-type(2)'), 1, {'autoAlpha':1, delay: 0.3});
		TweenLite.to($('#rules-section .span12 .row:first').find('.span4:nth-of-type(3)'), 1, {'autoAlpha':1, delay: 0.6});
		TweenLite.to($('#rules-section .span12 .row:last').find('.span4:nth-of-type(1)'), 1, {'autoAlpha':1, delay: 0.9});
		TweenLite.to($('#rules-section .span12 .row:last').find('.span4:nth-of-type(2)'), 1, {'autoAlpha':1, delay: 1.2});
		TweenLite.to($('#rules-section .span12 .row:last').find('.span4:nth-of-type(3)'), 1, {'autoAlpha':1, delay: 1.5});


	} else {
		$('#rules-section .span4').css({'visibility': 'visible', 'opacity': 1});
	}
}



/*
function workCardClickHandler() {

	$('.mobileMenu').hide();

	disableScrolling();

	$('.workCard').unbind('click');

	var $target = $(this);
	var targetId = $(this).attr('id');

	selectedFeaturedWork = targetId;

	if($('.workCard').filter('#' + selectedFeaturedWork).hasClass('last')) {

		TweenLite.to($('#contentWrapper'), 1, {left: '3000px', autoAlpha: 0, ease:Power2.easeOut});

  	} else {

  		TweenLite.to($('#contentWrapper'), 1, {left: '-2000px', autoAlpha: 0, ease:Power2.easeOut});
  	}

	// delay preloading a bit to allow content disappear for new preloader
	setTimeout(function() {

		shapesPreloader = new ShapesPreloader(function() {

				shapesPreloader.destroy();
				preloaderManifests[selectedFeaturedWork].isLoaded = true;
				enableSlidesBackButton();
				enableSlidesNavigation();

			 	if(!isTouch)
 					window.location.hash = '#' + $('.workCard').filter('#'+selectedFeaturedWork).attr('data-hashlink');

			}, {color: '#f2f2f2'});

		var preloader = new createjs.LoadQueue();
		currentManifestfilesLoaded = 0;
		currentManifestfilesToLoad = preloaderManifests[selectedFeaturedWork].urls.length;

		preloader.addEventListener("fileload", function(event) {

				currentManifestfilesLoaded++;

				loadedImages[event.item.src] = event.result;

				var progress = currentManifestfilesLoaded/currentManifestfilesToLoad;
				shapesPreloader.updateProgress(progress);

				// if first image is ready, show work
				if(event.item.src == preloaderManifests[selectedFeaturedWork].urls[0])
					openFeaturedWork(true);
			}
		);

		preloader.setMaxConnections(1);
		preloader.loadManifest(preloaderManifests[selectedFeaturedWork].urls);


	}, 1000);

	// hide header and text
	TweenLite.to($('#header'), 1, {top: '-120px', ease:Power2.easeOut, delay: 0.2});

	if(!isTouch) {

		TweenLite.to($('#featuredWorkHeader'), 1, {autoAlpha: '0', ease:Power2.easeOut, delay: 0.2});
		TweenLite.to($('#featuredWorkSubheader'), 1, {autoAlpha: '0', ease:Power2.easeOut, delay: 0.2});

	} else {

		$('#featuredWorkHeader').css({'visibility':'hidden'});
		$('#featuredWorkSubheader').css({'visibility':'hidden'});

	}


}

function openFeaturedWork(fromPreloader) {

	var template = featuredWorkTemplates[selectedFeaturedWork];

	$selectedFeaturedWorkTpl = $($.parseHTML(template())).filter('.workSlide');

	selectedFeaturedWorkCurrentSlide = 0;
	selectedFeaturedWorkSlidesNum 	 = $selectedFeaturedWorkTpl.length;

	$workSlidesHolder.show();

	initialSlide(fromPreloader);

}

function initialSlide(immediately) {


	$selectedFeaturedWorkCurrentSlide = $selectedFeaturedWorkTpl.first().clone();
	var $slideContent = $selectedFeaturedWorkCurrentSlide.find('.slideContent');

	$workSlidesHolder.append($selectedFeaturedWorkCurrentSlide);
	$('.anystretch, .fullscreen').width(stageWidth).height(stageHeight);

	var delay = 0.5;

	if(immediately) {
		delay = 0;
	}

	if(isCSSTrans) {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, delay, {'x': stageWidth}, {'x': 0});
		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide.find('.slideContent'), delay, {'x':stageWidth}, {'x': 0, delay: delay});

	} else {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, delay, {'left': stageWidth + 'px'}, {'left':'0px'});
		TweenLite.fromTo($slideContent, delay, {'left': stageWidth + 'px'}, {'left':'50%', delay: delay	});

	}


	if($selectedFeaturedWorkCurrentSlide.hasClass('anystretch')) {
		$selectedFeaturedWorkCurrentSlide.anystretch(loadedImages[$selectedFeaturedWorkCurrentSlide.attr('data-stretch')]);
	}

	selectedFeaturedWorkCurrentSlide = 0;

}

function nextSlide() {

	if(isSlideAnimationBusy) return;
	isSlideAnimationBusy = true;

	if(selectedFeaturedWorkCurrentSlide + 1 == selectedFeaturedWorkSlidesNum) {

		selectedFeaturedWorkCurrentSlide = 0;

	} else {

		selectedFeaturedWorkCurrentSlide++;

	}

	$slideToDestroy = $selectedFeaturedWorkCurrentSlide;

	$selectedFeaturedWorkCurrentSlide = $selectedFeaturedWorkTpl.eq(selectedFeaturedWorkCurrentSlide).clone();

	var $slideContent = $selectedFeaturedWorkCurrentSlide.find('.slideContent');

	if(isCSSTrans) {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, 0.5, {'x': stageWidth}, {'x': 0});
		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide.find('.slideContent'), 0.5, {'x':stageWidth}, {'x': 0, delay: 0.2, onComplete: slideChangeCompleteHandler });

	} else {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, 0.5, {'left': stageWidth + 'px'}, {'left':'0px'});
		TweenLite.fromTo($slideContent, 0.5, {'left': stageWidth + 'px'}, {'left':'50%', delay: 0.2, onComplete: slideChangeCompleteHandler});
	}

	$workSlidesHolder.append($selectedFeaturedWorkCurrentSlide);
	$selectedFeaturedWorkCurrentSlide.css('z-index', zIndexCounter);

	$slideContent.find('.slideBackButton').bind('click', closeFeaturedWork);
	$slideContent.find('.sayHelloButton').bind('click', {targetSection: 'connect'}, closeFeaturedWork);

	$('.anystretch, .fullscreen').width(stageWidth).height(stageHeight);

	if($selectedFeaturedWorkCurrentSlide.hasClass('anystretch')) {
		$selectedFeaturedWorkCurrentSlide.anystretch(loadedImages[$selectedFeaturedWorkCurrentSlide.attr('data-stretch')]);
	}

	zIndexCounter++;

}

function prevSlide() {

	if(isSlideAnimationBusy) return;
	isSlideAnimationBusy = true;

	if(selectedFeaturedWorkCurrentSlide == 0) {

		selectedFeaturedWorkCurrentSlide = selectedFeaturedWorkSlidesNum - 1;

	} else {

		selectedFeaturedWorkCurrentSlide--;

	}

	$slideToDestroy = $selectedFeaturedWorkCurrentSlide;

	$selectedFeaturedWorkCurrentSlide = $selectedFeaturedWorkTpl.eq(selectedFeaturedWorkCurrentSlide).clone();

	var $slideContent = $selectedFeaturedWorkCurrentSlide.find('.slideContent');

	if(isCSSTrans) {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, 0.5, {'x': -stageWidth}, {'x': 0});
		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide.find('.slideContent'), 0.5, {'x':-stageWidth}, {'x': 0, delay: 0.2, onComplete: slideChangeCompleteHandler });

	} else {

		TweenLite.fromTo($selectedFeaturedWorkCurrentSlide, 0.5, {'left': -stageWidth + 'px'}, {'left':'0px'});
		TweenLite.fromTo($slideContent, 0.5, {'left': -stageWidth + 'px'}, {'left':'50%', delay: 0.2, onComplete: slideChangeCompleteHandler});
	}

	$workSlidesHolder.append($selectedFeaturedWorkCurrentSlide);
	$selectedFeaturedWorkCurrentSlide.css('z-index', zIndexCounter);

	$slideContent.find('.slideBackButton').bind('click', closeFeaturedWork);
	$slideContent.find('.sayHelloButton').bind('click', {targetSection: 'connect'}, closeFeaturedWork);

	$('.anystretch, .fullscreen').width(stageWidth).height(stageHeight);

	if($selectedFeaturedWorkCurrentSlide.hasClass('anystretch')) {
		$selectedFeaturedWorkCurrentSlide.anystretch(loadedImages[$selectedFeaturedWorkCurrentSlide.attr('data-stretch')]);
	}

	zIndexCounter++;

}

function slideChangeCompleteHandler() {

	TweenLite.killTweensOf($selectedFeaturedWorkCurrentSlide.find('.slideContent'));
	TweenLite.killTweensOf($selectedFeaturedWorkCurrentSlide);

	TweenLite.killTweensOf($slideToDestroy.find('.slideContent'));
	TweenLite.killTweensOf($slideToDestroy);

	$slideToDestroy.remove();
	isSlideAnimationBusy = false;

}

function enableSlidesBackButton() {

	TweenLite.to($('#workSlidesBack'), 0.5, {top: '0px', ease:Power2.easeOut});
	$('#workSlidesBack').bind('click', closeFeaturedWork);
}

function disableSlidesBackButton() {

	TweenLite.to($('#workSlidesBack'), 0.5, {top: '-100px', ease:Power2.easeOut});
	$('#workSlidesBack').unbind('click');
}

function enableSlidesNavigation() {

	$('#workSlidesNext').css('display', 'block');
	$('#workSlidesPrev').css('display', 'block');

	TweenLite.to($('#workSlidesPrev'), 0.5, {left: '20px', ease:Power2.easeOut});
	TweenLite.to($('#workSlidesNext'), 0.5, {right: '20px', ease:Power2.easeOut});

	$('#workSlidesPrev').bind('click', prevSlide);
	$('#workSlidesNext').bind('click', nextSlide);
}

function disableSlidesNavigation() {

	TweenLite.to($('#workSlidesPrev'), 0.5, {left: '-100px', ease:Power2.easeOut});
	TweenLite.to($('#workSlidesNext'), 0.5, {right: '-100px', ease:Power2.easeOut, onComplete: function() {

		$('#workSlidesNext').css('display', 'none');
		$('#workSlidesPrev').css('display', 'none');

	}});

	$('#workSlidesPrev').unbind('click');
	$('#workSlidesPrev').unbind('click');
}


function closeFeaturedWork(event) {

	disableSlidesNavigation();
	disableSlidesBackButton();

	if(!isTouch) {

		TweenLite.to($('#featuredWorkHeader'), 0, {autoAlpha: '1', ease:Power2.easeOut});
		TweenLite.to($('#featuredWorkSubheader'), 0, {autoAlpha: '1', ease:Power2.easeOut});

	} else {

		$('#featuredWorkHeader').css({'visibility':'visible'});
		$('#featuredWorkSubheader').css({'visibility':'visible'});

	}

	$workSlidesHolder.hide();

	$('#'+selectedFeaturedWork).find('.viewButton').html('View');

	$('.workCard:not(#'+selectedFeaturedWork+')').css({opacity: 0});
	$('.workCard').removeClass('active');

	TweenLite.to($('#header'), 1, {top: '0px', ease:Power2.easeOut});

	TweenLite.to($('#contentWrapper'), 0.5, {
		left: '0px',
		autoAlpha : 1,
		ease:Power2.easeOut,
		onComplete: function() {

			if(!isTouch) {

				TweenLite.to($('.workCard:not(#'+selectedFeaturedWork+')'), 0.5, {'autoAlpha':1});

			} else {

				$('.workCard:not(#'+selectedFeaturedWork+')').css({'visibility': 'visible', 'opacity':1});
			}

			//$('.workCard').bind('click', workCardClickHandler);

			$workSlidesHolder.empty();

			enableScrolling();
		}
	});


	if(!isTouch)
 		window.location.hash = '#work';

	if(event.data && event.data.targetSection) {

		var targetSection = sections[event.data.targetSection];
		scrollTarget = targetSection.startOffset;

	} else {

		scrollTarget = sections.work.startOffset;

	}



}

*/



/*
function initTwitterFeed() {

	$("#twitterFeed").tweet({
			join_text: "auto",
			avatar_size: 0,
			username: "brendanjarvis",
			filter: function(t){ return ! /^@\w+/.test(t.tweet_raw_text); },
			count: 5,
			fetch: 20,
			loading_text: "",
			template: '<div class="message"><span class="date">{space_time}</span>{text}</div>'
		}).bind('loaded', function(){

			$loadedTweetsList = $('.tweet_list li');
			$loadedTweetsList.hide();
			$loadedTweetsList.eq(currentTweetIndex).fadeIn('slow');

			clearTimeout(changeTweetInterval);
			changeTweetInterval = setTimeout(changeTweet, 7000);

		});
}

function changeTweet() {

	$loadedTweetsList.eq(currentTweetIndex).fadeOut((isTouch ? 0 : 'slow'), function(){

		if(currentTweetIndex < $loadedTweetsList.length-1)
			currentTweetIndex++;
		else
			currentTweetIndex = 0;

		$loadedTweetsList.stop();
		$loadedTweetsList.eq(currentTweetIndex).fadeIn('slow');

		changeTweetInterval = setTimeout(changeTweet, 7000);

	});
}


*/
$(function(){
    // basic usage
    $('a.normalTip').aToolTip();

    // fixed tooltip
    $('a.fixedTip').aToolTip({
        fixed: true
    });

    // on click tooltip with custom content
    $('a.clickTip').aToolTip({
        clickIt: true,
        fixed: true,
        yOffset: 5,
        xOffset: -90,
        tipContent: '<h4>Online-Showroom</h4> <hr> <p><b>coming soon...</b></p>'
    });

    // on click tooltip that has callback functions
    $('a.callBackTip').aToolTip({
        clickIt: true,
        fixed: false,
        yOffset: 5,
        xOffset: 1,
        onShow: function(){alert('I fired OnShow')},
        onHide: function(){alert('I fired OnHide')}
    });
});

$('.carousel').carousel(
{
	pause: true,
	interval: false
});
$('.bxslider').bxSlider({
  mode: 'fade',
  captions: false,
  pager: false,
  controls: false,
  autoStart: true,
  auto: true,
  speed: 600
});

$("#vimeo").click(function() {
	$.fancybox({
		'padding'		: 0,
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'title'			: this.title,
		'width'			: 700,
		'height'		: 400,
		'href'			: this.href.replace(new RegExp("([0-9])","i"),'moogaloop.swf?clip_id=$1'),
		'type'			: 'swf'
	});

	return false;
});
